const request = require('request');

const firebase = require("firebase");
require('firebase/firestore')



// Set the configuration for your app
// TODO: Replace with your project's config object
var config = {
    apiKey: " AIzaSyBV3ARQOtmzUP2iK4AGKFaSgee0MbhOM7A ",
    authDomain: "quran-hifz-bot.firebaseapp.com",
    databaseURL: "https://quran-hifz-bot.firebaseio.com",
    projectId: "quran-hifz-bot"
};

firebase.initializeApp(config)
const database = firebase.database()

const students = database.ref('students/');

const bot_token = '552896946:AAFytAD8rWkmnOScP7nZL_C54GYLbNFjNvY'

const message = process.argv[2]
console.log('Message '+message)

students.once('value').then(snapshot => {
    snapshot.forEach( student => {
        if (student.child('chat_id').exists()) {
            console.log('Sending message to '+student.key+' '+student.child('name').val())
            const chat_id = student.child('chat_id').val()

            request('https://api.telegram.org/bot'+bot_token+'/sendMessage?chat_id='+chat_id+'&parse_mode=Markdown&text='+message,
            (err, rsp, body) => {
                if (err || rsp.statusCode != 200) {
                    console.log(err, body)
                }
            })
        }
    })
})

