// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const firebase = require('firebase-admin')
import * as Strings from './strings';

// give us the possibility of manage request properly
const app = express()

interface Keyboard {
    keyboard?: { text: string }[][],
    one_time_keyboard?: boolean,
    remove_keyboard?: boolean
}

interface Reply {
    method: string,
    chat_id: number,
    text: string,
    reply_markup?: Keyboard,
}

const states = {
    BASE: 0,
    REPORT_TYPE: 1,
    REPORT_REV_COUNT: 2,
    REPORT_MEM_COUNT: 3,
    GOAL_TYPE: 4,
    GOAL_REV_COUNT: 5,
    GOAL_MEM_COUNT: 6
}

const category_keyboard: Keyboard = {
    keyboard: [Strings.actv_name.map((name) => ({text: name}))],
    one_time_keyboard: true
}

const remove_keyboard: Keyboard = {
    remove_keyboard: true
}
/****************************************
 * 
 *  TODO generalize this stuff
 * 
 ****************************************/

const epoch = new Date('2019-09-19')

// Set the configuration for your app
// TODO: Replace with your project's config object
var config = {
    apiKey: " AIzaSyBV3ARQOtmzUP2iK4AGKFaSgee0MbhOM7A ",
    authDomain: "quran-hifz-bot.firebaseapp.com",
    databaseURL: "https://quran-hifz-bot.firebaseio.com",
};
firebase.initializeApp(config);

// Get a reference to the database service
var database = firebase.database();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }))

// our single entry point for every message
app.post('/', async (req, res) => {
    const message = req.body.message? req.body.message: req.body.edited_message

    const chat_id = message.chat.id
    const { first_name, id } = message.from
    const text = message.text ? message.text : ''
    const students = database.ref('students/');
    const student = database.ref('students/' + id);
    const default_reply: Reply = {
        method: 'sendMessage',
        chat_id,
        text: Strings.err_not_cmd(),
        reply_markup: remove_keyboard
    }

    const now = new Date()
    const day = Math.floor((now.getTime() - epoch.getTime())/(1000 * 60 * 60 * 24))
    const week = Math.floor(day/7)

    var reply: Reply = default_reply;
    student.once('value').then(snapshot => {
        if (!snapshot.exists()) {
            // add new student
            snapshot.ref.set({
                nickname: 'default',
                name: first_name,
                dfa_state: states.BASE,
                points: 0,
                passes: 4,  
            })
        }
    }).then(() => Promise.all([student.once('value'), students.once('value')]))
    .then(([snapshot, students]) => {
        const state = snapshot.child('dfa_state').val()
        if (text.startsWith('/report')) {
            if (snapshot.child('weekly_goals/'+week).exists()) {
                reply = {method: 'sendMessage', chat_id, 
                    text: Strings.what_report_type(),
                    reply_markup: category_keyboard}
                snapshot.ref.child('dfa_state').set(states.REPORT_TYPE)
            } else {
                reply = {method: 'sendMessage', chat_id,
                    text: Strings.err_no_goal()}
            }            
        } else if (text.startsWith('/setweeklygoal')) {
            reply = {method: 'sendMessage', chat_id,
                text: Strings.what_goal_type(),
                reply_markup: category_keyboard}
            snapshot.ref.child('dfa_state').set(states.GOAL_TYPE)
        } else if (text.startsWith('/recitedtomentor')) {
            reply = {method: 'sendMessage', chat_id,
                text: Strings.success_recitedtomentor(250)}
            snapshot.child('points').ref.set(Number(snapshot.child('points').val())+250)
            const entry = snapshot.child('recitations/'+day)
            entry.ref.set(Number(entry.val()) ? entry.val() + 1 : 1)
            snapshot.ref.child('dfa_state').set(states.BASE)
        } else if (text.startsWith('/setnickname')) {
            reply = setnickname_handler(text, chat_id, snapshot)
            snapshot.ref.child('dfa_state').set(states.BASE)
        } else if (text.startsWith('/help')) {
            reply = help_handler(text, chat_id, snapshot)
            snapshot.ref.child('dfa_state').set(states.BASE)
        } else if (text.startsWith('/start')) {
            reply = {method: 'sendMessage', chat_id, text: Strings.welcome_msg()}
            snapshot.ref.child('dfa_state').set(states.BASE)
        } else if (text.startsWith('/leaderboard')) {
            reply = leaderboard_handler(chat_id, snapshot, students)
            snapshot.ref.child('dfa_state').set(states.BASE)
        } else if (text.startsWith('/status')) {
            reply = status_handler(day, week, chat_id, snapshot)
            snapshot.ref.child('dfa_state').set(states.BASE)
        } else if (state == states.REPORT_TYPE) {
            // sanity check
            if (text != 'Memorization' && text != 'Revision') {
                reply = {method: 'sendMessage', chat_id, reply_markup: remove_keyboard,
                text: Strings.err_not_type()}
                snapshot.ref.child('dfa_state').set(states.BASE)
            } else {
                const [units, actv, new_state] = (text == 'Memorization')? 
                    ['verses', Strings.Activity.Mem, states.REPORT_MEM_COUNT]: 
                    ['pages', Strings.Activity.Rev, states.REPORT_REV_COUNT]
                reply = {method: 'sendMessage', chat_id, reply_markup: remove_keyboard,
                    text: Strings.report_qnt(units, actv)}
                snapshot.ref.child('dfa_state').set(new_state)
            }
        } else if (state == states.GOAL_TYPE) {
            if (text != 'Memorization' && text != 'Revision') {
                reply = {method: 'sendMessage', chat_id, reply_markup: remove_keyboard,
                text: Strings.err_not_type()}
                snapshot.ref.child('dfa_state').set(states.BASE)
            } else {
                const [units, actv, new_state] = (text == 'Memorization')? 
                    ['verses', Strings.Activity.Mem, states.GOAL_MEM_COUNT]: 
                    ['pages', Strings.Activity.Rev, states.GOAL_REV_COUNT]
                reply = {method: 'sendMessage', chat_id, reply_markup: remove_keyboard,
                    text: Strings.goal_qnt(units, actv)}
                snapshot.ref.child('dfa_state').set(new_state)
            }
        } else if (state == states.REPORT_MEM_COUNT) {
            
            /***********************************************************
             * This branch is to be factored into report_handler below *
             ***********************************************************/
            
            if (isNaN(text)) {
                reply = {method: 'sendMessage', chat_id, text: Strings.err_not_num()}
            } else if (Number(text) < 1) {
                reply = {method: 'sendMessage', chat_id, text: Strings.err_atleast(1, "verse")}
            } else {
                const n = Number(text)
                var extra_msg = ''
                var new_points = 0
                var report = snapshot.child('reports/'+day)
                var mem = snapshot.child('reports/'+day+'/mem')
                var rev = snapshot.child('reports/'+day+'/rev')
                // no existing report or reports as 0
                if (!report.exists() || (Number(mem.val()) + Number(rev.val()) == 0)) {
                    new_points += 50
                }
                
                // calculate week memorization
                const week_start = week * 7
                var week_rev = 0, week_mem = 0;
                snapshot.child('reports').forEach(child => {
                    const { mem, rev } = child.val()
                    if (child.key >= week_start) {
                        week_mem += mem? mem: 0
                        week_rev += rev? rev: 0
                    }
                })
                
                var weekly_goals = snapshot.child('weekly_goals/' + week) 
                // Note: weekly_goals should be guaranteed to exist, 
                // you can't report if the weekly goals aren't set

                // Number(null) == 0, so we don't need to case for goal type not existing
                var mem_goals = Number(weekly_goals.child('mem').val())
                var rev_goals = Number(weekly_goals.child('rev').val())
                var mem_goals_met = false
                var rev_goals_met = false

                // check if memorization goal is met
                if (week_mem + n >= mem_goals)
                        mem_goals_met = true
                // check if revision goal is met
                if (week_rev >= rev_goals)
                        rev_goals_met = true

                if (mem_goals+rev_goals*2 < 7) {
                    mem_goals_met = false
                    rev_goals_met = false
                }
                
                if (rev_goals_met && mem_goals_met) {
                    if (n > 0) {
                        if (week_mem < mem_goals) {
                            // this report reached the goal
                            extra_msg = `Congrats, ${snapshot.child('name').val()}. You reached your weekly goal. You get extra 1000 points!\n`
                            new_points += 1000
                            var bonus = (week_mem + n - mem_goals)

                            var rev_text = ''
                            var rev_bonus = (week_rev - rev_goals > 0) ? week_rev - rev_goals : 0
                            if (rev_bonus) rev_text = ` and revised ${rev_bonus} extra pages`
                            bonus += rev_bonus
                            if (bonus > 0) {
                                new_points += bonus*100
                                extra_msg += `Since you memorized ${bonus - rev_bonus} extra verses` + rev_text + ` after you meet your weekly goal, you get ${bonus*100} extra points!⚡️⚡️\n`
                            }
                        } else { // goal reached before
                            new_points += n*100
                        }
                    }
                }

                snapshot.child('points').ref.set(snapshot.child('points').val() + new_points)
                const entry = snapshot.child('reports/'+day+'/mem')
                entry.ref.set(entry.exists()? entry.val()+n: n)

                extra_msg += `Use /status to check your points.`
                reply = {method: 'sendMessage', chat_id, text: 'Success! ' + extra_msg}
                snapshot.ref.child('dfa_state').set(states.BASE)
            }
        } else if (state == states.REPORT_REV_COUNT) {

            /***********************************************************
             * This branch is to be factored into report_handler below *
             ***********************************************************/
            
            if (isNaN(text)) {
                reply = {method: 'sendMessage', chat_id, text: 'Not a number! try again.'}
            } else if (Number(text) < 0.5) {
                reply = {method: 'sendMessage', chat_id, text: 'Must be at least 0.5 pages!'}
            } else {
                const n = Number(text)
                var extra_msg = ''
                var new_points = 0
                var report = snapshot.child('reports/'+day)
                var mem = snapshot.child('reports/'+day+'/mem')
                var rev = snapshot.child('reports/'+day+'/rev')
                // no existing report or reports as 0
                if (!report.exists() || (Number(mem.val()) + Number(rev.val()) == 0)) {
                    new_points += 50
                }
                
                // calculate week memorization & revision
                const week_start = week * 7
                var week_rev = 0, week_mem = 0;
                snapshot.child('reports').forEach(child => {
                    const { mem, rev } = child.val()
                    if (child.key >= week_start) {
                        week_mem += mem? mem: 0
                        week_rev += rev? rev: 0
                    }
                })
                
                var weekly_goals = snapshot.child('weekly_goals/' + week) 
                // Note: weekly_goals should be guaranteed to exist, 
                // you can't report if the weekly goals aren't set

                // Number(null) == 0, so we don't need to case for goal type not existing
                var mem_goals = Number(weekly_goals.child('mem').val())
                var rev_goals = Number(weekly_goals.child('rev').val())
                var mem_goals_met = false
                var rev_goals_met = false

                // check if memorization goal is met
                if (week_mem >= mem_goals)
                        mem_goals_met = true
                // check if revision goal is met with the new revision
                if (week_rev + n >= rev_goals)
                        rev_goals_met = true
                
                if (mem_goals+rev_goals*2 < 7) {
                    mem_goals_met = false
                    rev_goals_met = false
                }

                if (rev_goals_met && mem_goals_met) {
                    if (n > 0) {
                        if (week_rev < rev_goals) {
                            // this report reached the goal
                            extra_msg = `Congrats, ${snapshot.child('name').val()}. You reached your weekly goal. You get extra 250 points!\n`
                            new_points += 250
                            var bonus = (week_rev + Math.floor(n) - rev_goals)

                            var mem_text = ''
                            var mem_bonus = (week_mem - mem_goals > 0) ? week_mem - mem_goals : 0
                            if (mem_bonus) mem_text = ` and memorized ${mem_bonus} extra verses`
                            bonus += mem_bonus
                            if (bonus > 0) {
                                new_points += bonus*100
                                extra_msg += `Since you revised ${bonus - mem_bonus} extra pages` + mem_text + ` after you meet your weekly goal, you get ${bonus*100} extra points!⚡️⚡️\n`
                            }
                        } else { // goal reached before
                            new_points += Math.floor(n)*100
                        }
                    }
                }

                snapshot.child('points').ref.set(snapshot.child('points').val() + new_points)
                const entry = snapshot.child('reports/'+day+'/rev')
                entry.ref.set(entry.exists()? entry.val()+n: n)

                extra_msg += `Use /status to check your points.`
                reply = {method: 'sendMessage', chat_id, text: 'Success! ' + extra_msg}
                snapshot.ref.child('dfa_state').set(states.BASE)
            }
        } else if (state == states.GOAL_MEM_COUNT || state == states.GOAL_REV_COUNT) {
            reply = set_goal_handler(week, state, text, chat_id, snapshot)
        }

        console.log(id, text, reply, state)
        if (reply != null) {
            if (reply.reply_markup == null) {
                reply.reply_markup == remove_keyboard
            }

            return res.status(200).send(reply)
        } else {
            return res.status(200).send(default_reply)
        }
    })
})


// TODO replace both REPORT_REV_COUNT & REPORT_MEM_COUNT branches with calls to this
function report_handler(text, chat_id, snapshot, day, week): Reply {

    // NOTE could be changed so invalid input request input again instead of
    // going back to base state
    snapshot.ref.child('dfa_state').set(states.BASE)

    if (isNaN(text)) {
        return {method: 'sendMessage', chat_id, text: Strings.err_not_num()}
    }
    
    if (Number(text) < 1) {
        const unit = (text == Strings.actv_name[Strings.Activity.Mem])
        /// XXX generic
        return {method: 'sendMessage', chat_id, text: Strings.err_atleast(1, "verse")}
    }

    const n = Number(text)
    var extra_msg = ''
    var new_points = 0
    var report = snapshot.child('reports/'+day)
    var mem = snapshot.child('reports/'+day+'/mem')
    var rev = snapshot.child('reports/'+day+'/rev')
    // no existing report or reports as 0
    if (!report.exists() || (Number(mem.val()) + Number(rev.val()) == 0)) {
        new_points += 50
    }
    
    // calculate week memorization
    const week_start = week * 7
    var week_rev = 0, week_mem = 0;
    snapshot.child('reports').forEach(child => {
        const { mem, rev } = child.val()
        if (child.key >= week_start) {
            week_mem += mem? mem: 0
            week_rev += rev? rev: 0
        }
    })
    
    var weekly_goals = snapshot.child('weekly_goals/' + week) 
    // Note: weekly_goals should be guaranteed to exist, 
    // you can't report if the weekly goals aren't set

    // Number(null) == 0, so we don't need to case for goal type not existing
    var mem_goals = Number(weekly_goals.child('mem').val())
    var rev_goals = Number(weekly_goals.child('rev').val())
    var mem_goals_met = false
    var rev_goals_met = false

    // check if memorization goal is met
    if (week_mem + n >= mem_goals)
            mem_goals_met = true
    // check if revision goal is met
    if (week_rev >= rev_goals)
            rev_goals_met = true

    if (mem_goals+rev_goals*2 < 7) {
        mem_goals_met = false
        rev_goals_met = false
    }
    
    if (rev_goals_met && mem_goals_met) {
        if (n > 0) {
            if (week_mem < mem_goals) {
                // this report reached the goal
                extra_msg = `Congrats, ${snapshot.child('name').val()}. You reached your weekly goal. You get extra 1000 points!\n`
                new_points += 1000
                var bonus = (week_mem + n - mem_goals)

                var rev_text = ''
                var rev_bonus = (week_rev - rev_goals > 0) ? week_rev - rev_goals : 0
                if (rev_bonus) rev_text = ` and revised ${rev_bonus} extra pages`
                bonus += rev_bonus
                if (bonus > 0) {
                    new_points += bonus*100
                    extra_msg += `Since you memorized ${bonus - rev_bonus} extra verses` + rev_text + ` after you meet your weekly goal, you get ${bonus*100} extra points!⚡️⚡️\n`
                }
            } else { // goal reached before
                new_points += n*100
            }
        }
    }

    snapshot.child('points').ref.set(snapshot.child('points').val() + new_points)
    const entry = snapshot.child('reports/'+day+'/mem')
    entry.ref.set(entry.exists()? entry.val()+n: n)

    extra_msg += `Use /status to check your points.`

}


function setnickname_handler(text, chat_id, snapshot) {
    // parse and verify
    const words = text.split(' ')
    if (words.length < 2) {
        return {
            method: 'sendMessage',
            chat_id,
            text: 'Use /setnickname <name> (without the brackets)'
        }
    }

    snapshot.ref.child('nickname').set(words[1])
    return {
        method: 'sendMessage',
        chat_id,
        text: `Your nickname is now set to ${words[1]}.`
    }
}

function set_goal_handler(week, state, text, chat_id, snapshot) {
    const cat = (state == states.GOAL_MEM_COUNT)? '/mem': '/rev'
    const entry = snapshot.child('weekly_goals/'+week+cat)
    if (entry.exists()) {
        const reply = {method: 'sendMessage', chat_id, 
            text: "You already set this week's goal for this category"}
        snapshot.child('dfa_state').ref.set(states.BASE)
        return reply
    } else {
        if (isNaN(text)) {
            return {method: 'sendMessage', chat_id, text: 'Not a number! try again.'}
        }
        entry.ref.set(Number(text))
        snapshot.child('dfa_state').ref.set(states.BASE)
        return {method: 'sendMessage', chat_id, text: 'Success'}
    }
}

function leaderboard_handler(chat_id, student, students) {
    var leaderboard = []
    students.forEach(function(student) {
        var {nickname, points} = student.val()
        var chat_id = student.key
        leaderboard.push({nickname, points, chat_id})
        }
    )
    leaderboard.sort((s1, s2) => s2.points - s1.points)

    var boardString = ""
    for(var i = 0; i < leaderboard.length; i++) {
        var s = leaderboard[i]
        if (student.key == s.chat_id) {
            s.nickname = `*${s.nickname}*`
        }
        
        if (i == 0 || i == 1 || i ==2) {
            boardString = boardString + `${i+1}. ${s.nickname} - ${s.points}` + "🔥".repeat(3-i) + "\n"
        } else {
            boardString = boardString + `${i+1}. ${s.nickname} - ${s.points}\n`
        }
    }

    return {
        method: 'sendMessage',
        chat_id,
        parse_mode: "Markdown",
        text: `✨*QUR'AN HIFZ LEADERBOARD*✨\n\nHi, ${student.child("name").val()}! Below is the leaderboard:\n\n` + boardString
    }
}

function help_handler(text, chat_id, snapshot) {
    return {
        method: 'sendMessage',
        chat_id,
        parse_mode: "Markdown",
        text: 
                `\nHi, ${snapshot.child("name").val()}! I'm Hifzi. Here's how to let me help you! 😊\n\n/report - Report your memorization/revision of the day\nIf you activate this command, you'll be asked on what you want to report. Choose either Memorization or Revision, then enter the number of verses/pages you memorize or recite, respectively.\n/status - Check your progress & status\n/setnickname - Set your nickname\nExample: /setnickname funkymonkey\n/setweeklygoal - Set your goal for the week\nThis is in terms of verses for memorization and pages for revision. The format is the same as reporting!You'll be able to set your new goal every Thursday!\n/leaderboard - See the leaderboard\n/help - Show help\n\nQUR'AN HIFZ SYSTEM\nPOINTS SYSTEM:\nDaily Report: 50 points\nRecited: 250 points\nReached Weekly Goal: 1000 points\nEvery verse (memorization)/page (revision) after reaching goals: 100 points\n\nCheck /leaderboard to see your ranking! There will be special prizes for the top 5 participants!\nRULES:\nReport EVERY DAY! At least one verse (memorization) or 0.5 pages (revision) are required. \nRecite once a week.\nRepeat ;)\n\nYou have 4 free passes, which means 4 days in which you don't report. It cannot be used consecutively! If you miss reporting for the 5th time, that's the finish line for you.\nHappy memorizing! May Allah accept it from us ☺️`  
    }
}

function status_handler(day, week, chat_id, snapshot) {
    const week_start = week * 7
    var total_mem = 0, total_rev = 0, week_mem = 0, week_rev = 0;
    snapshot.child('reports').forEach(child => {
        const { mem, rev } = child.val()
        total_mem += mem? mem: 0
        total_rev += rev? rev: 0
        if (child.key >= week_start) {
            week_mem += mem? mem: 0
            week_rev += rev? rev: 0
        }
    })
    const today_mem = Number(snapshot.child('reports/'+day+'/mem').val())
    const today_rev = Number(snapshot.child('reports/'+day+'/rev').val())
    const date = new Date().toDateString()
    var text = `Hi, ${snapshot.child('name').val()}! This is your status as of ${date}\n\n`
    var points = Number(snapshot.child('points').val())
    text += `Total points: *${points}*\n`

    var mem = snapshot.child('weekly_goals/'+week+"/mem")
    var rev = snapshot.child('weekly_goals/'+week+"/rev")
    var passes = snapshot.child('passes')
    if (mem.exists())  {
        if (rev.exists())
            text += `This week, you have set to memorize *${mem.val()}* verses and revise *${rev.val()}* pages!\n`
        else
            text += `This week, you have set to memorize *${mem.val()}* verses. No revision goal is set!\n`
    } else if (rev.exists()) {
        if (!mem.exists())
            text += `This week, you have set to revise *${rev.val()}* pages. No memorization goal is set!\n`
        else
            text += `You haven't set your goals this week! Use /setweeklygoals to set it.\n`
    }
    text += `\nTotal (During the program):\nMemorized: ${total_mem} verses\nRevised: ${total_rev} pages\n\n`
    text += `This Week:\nMemorized: ${week_mem} verses\nRevised: ${week_rev} pages\n\n`
    text += `Today:\nMemorized: ${today_mem} verses\nRevised: ${today_rev} pages`
    if (passes.exists())
        text += `\nYou currently have *${passes.val()}* passes left!`
    return {method: 'sendMessage', 
            chat_id, 
            parse_mode:"markdown", 
            text}
}

// this is the only function it will be published in firebase
exports.router = functions.https.onRequest(app)
