const request = require('request');

const firebase = require("firebase");
require('firebase/firestore')

// Set the configuration for your app
// TODO: Replace with your project's config object
var config = {
    apiKey: " AIzaSyBV3ARQOtmzUP2iK4AGKFaSgee0MbhOM7A ",
    authDomain: "quran-hifz-bot.firebaseapp.com",
    databaseURL: "https://quran-hifz-bot.firebaseio.com",
    projectId: "quran-hifz-bot"
};

firebase.initializeApp(config)
const database = firebase.database()

const students = database.ref('students/');

const bot_token = '552896946:AAFytAD8rWkmnOScP7nZL_C54GYLbNFjNvY'

const epoch = new Date('2019-09-19')
const now = new Date()
const day = Math.floor((now - epoch)/(1000 * 60 * 60 * 24))


students.once('value').then(snapshot => {
    snapshot.forEach( student => {
        if(!student.child('exempt').exists()) {
            if (!student.child('reports/' + day).exists()) {
                const chat_id = student.key
                var message = "Hi!\n\nI think you haven't reported your memorization nor revision today. Don't forget to report!\n"
                console.log('Message '+ message)
                console.log('Sending reminder to '+student.key+' '+student.child('name').val())
                
                // if (student.key == "523298322") { // Akhyar's testing purposes
                if (!student.child('reports/' + (day-1)).exists()) {
                    console.log('Sending warning to '+student.key+' '+student.child('name').val())
                    student.child('passes').ref.set(student.child('passes').val()-1)
                    message += `\nAnd I noticed you didn't report yesterday! If you don't report today, you'll be officially out of the program.\n\nYou can still use me to help you memorize, but your mentors will not be obliged to schedule a session for you, and your ranking will disappear from the leaderboard.\n\nI really hope you can report one verse memorization or half a page revision today 😥😥\n\nIf you think this is a mistake, contact @akhyarkamili`
                }  
                request.post('https://api.telegram.org/bot'+bot_token+'/sendMessage', {
                        json: {                      
                            method: 'sendMessage',
                            chat_id,
                            parse_mode: 'Markdown',
                            text: message
                        }
                    },
                    (err, rsp, body) => {
                        if (err || rsp.statusCode != 200) {
                            console.log(err, body)
                        }
                    })           
            }
        } else {
            console.log (`${student.child('name').val()} is exempt`)
            student.child('exempt').ref.set(student.child('exempt').val() - 1)
            if (student.child('exempt').val() - 1 == 0) {
                student.child('exempt').ref.remove()
                .then(function() {
                  console.log(`Exemption removal for ${student.key} succeeded.`)
                })
                .catch(function(error) {
                  console.log("Remove failed: " + error.message)
                });
            }
        }
    })
})

