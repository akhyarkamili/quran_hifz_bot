export enum Activity {
    Mem,
    Rev,
}

export type Values<T> = {
    [key in Activity]: T;
};

export var actv_name: string[] = ['memorization', 'revision'];
export var verb: string[] = ['memorize', 'revise'];

export const what_report_type = () => "What would you like to report?";
export const what_goal_type = () => "Which goal would you like to set?";
export const success_recitedtomentor = (pts: number) => 
    `Successfully reported recitation to mentor. You get ${pts} points!`
export const welcome_msg = () => "Welcome! Type /help to show help";
export const report_qnt = (units: string, type: Activity) => 
    `How many ${units} did you ${verb[type]}? (Write a number only)`;
export const goal_qnt = (units: string, type: Activity) => 
    `How many ${units} do you want to ${verb[type]} this week? (Write a number only)`
export const goal_reached = (name: string, pts: number) => 
    `Congrats, ${name}. You reached your weekly goal. You get extra ${pts} points!\n`;

export const err_not_cmd = () => "Sorry, I didn't understand that!";
export const err_no_goal = () => "You didn't set your weekly goal yet. Use /setweeklygoal";
export const err_atleast = (count: number, unit: string) => `Must be at least ${count} ${unit}!`;
export const err_not_num = () => 'Not a number! try again.';
export const err_not_type = () => "Whoops! You have to choose between Memorization and Revision! Operation cancelled.";
export const err_goalset = () => "You already set this week's goal for this category";

// TODO: generic activity types
export const bonus_report = (bonus: Values<number>, weights: Values<number>) =>
    `Since you memorized ${bonus[Activity.Mem]} extra verses` +
    `and revised ${bonus[Activity.Rev]} extra pages after you met your weekly goal,` +
    `you get ${bonus[Activity.Rev]*weights[Activity.Rev] + bonus[Activity.Mem]*weights[Activity.Mem]} extra points!⚡️⚡️\n`;

export const setnickname_msg = () => 'Use /setnickname <name> (without the brackets)';
export const success_nickname = (name: string) => `Your nickname is now set to ${name}.`;

export const leaderboard = (name: string, boardString: string) => `✨*QUR'AN HIFZ LEADERBOARD*✨\n\nHi, ${name}! Below is the leaderboard:\n\n ${boardString}`;

// TODO: generic types and points
export const help_msg = (name: string) => `\nHi, ${name}! I'm Hifzi. Here's how to let me help you! 😊\n\n/report - Report your memorization/revision of the day\nIf you activate this command, you'll be asked on what you want to report. Choose either Memorization or Revision, then enter the number of verses/pages you memorize or recite, respectively.\n/status - Check your progress & status\n/setnickname - Set your nickname\nExample: /setnickname funkymonkey\n/setweeklygoal - Set your goal for the week\nThis is in terms of verses for memorization and pages for revision. The format is the same as reporting!You'll be able to set your new goal every Thursday!\n/leaderboard - See the leaderboard\n/help - Show help\n\nQUR'AN HIFZ SYSTEM\nPOINTS SYSTEM:\nDaily Report: 50 points\nRecited: 250 points\nReached Weekly Goal: 1000 points\nEvery verse (memorization)/page (revision) after reaching goals: 100 points\n\nCheck /leaderboard to see your ranking! There will be special prizes for the top 5 participants!\nRULES:\nReport EVERY DAY! At least one verse (memorization) or 0.5 pages (revision) are required. \nRecite once a week.\nRepeat ;)\n\nYou have 4 free passes, which means 4 days in which you don't report. It cannot be used consecutively! If you miss reporting for the 5th time, that's the finish line for you.\nHappy memorizing! May Allah accept it from us ☺️`;

// TODO: complete
export const status_msg = (name: string, pts: number, status: Values<number>) => `Hi, ${name}! This is your status as of ${Date()}\n\n` +
    `Total points: *${pts}*\n`;
