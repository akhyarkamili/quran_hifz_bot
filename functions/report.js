const request = require('request');

const firebase = require("firebase");
require('firebase/firestore')

// Set the configuration for your app
// TODO: Replace with your project's config object
var config = {
    apiKey: " AIzaSyBV3ARQOtmzUP2iK4AGKFaSgee0MbhOM7A ",
    authDomain: "quran-hifz-bot.firebaseapp.com",
    databaseURL: "https://quran-hifz-bot.firebaseio.com",
    projectId: "quran-hifz-bot"
};

firebase.initializeApp(config)
const database = firebase.database()

const students = database.ref('students/');
var report = []

students.once('value').then(snapshot => {
    snapshot.forEach(student => {
        var rep = {
            id: student.key,
            nickname: student.child('nickname').val(),
            name: student.child('name').val(),
            points: student.child('points').val(),
            rev: 0, mem: 0
        }

        student.child('reports').forEach(day => {
            const revised = day.child('rev')
            const memorised = day.child('mem')
            if (revised.exists()) {
                rep.rev += revised.val()
            }
            if (memorised.exists()) {
                rep.mem += memorised.val()
            }
        })
        report.push(rep)
    })
}).then(done => {
    report.forEach(s => {
        console.log([s.id, s.name, s.nickname, s.points, s.mem, s.rev].join(', '))
    })
})

